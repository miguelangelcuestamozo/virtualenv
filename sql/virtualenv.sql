-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 06-06-2020 a las 16:07:13
-- Versión del servidor: 8.0.20-0ubuntu0.20.04.1
-- Versión de PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `virtualenv`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int NOT NULL,
  `user_name` varchar(128) NOT NULL,
  `nombre` varchar(128) NOT NULL,
  `apellidos` varchar(128) NOT NULL,
  `correo` varchar(128) NOT NULL,
  `contraseña` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `user_name`, `nombre`, `apellidos`, `correo`, `contraseña`) VALUES
(1, 'asfd', 'asdf', 'asdf', 'asdf@a.s', 'asdf'),
(6, 'zavodca', 'miguel', 'root', 'hola@hola.es', '12345678'),
(7, 'zavodca', 'miguel', 'root', 'hola@hola.es', '12345678'),
(8, 'zavodca', 'miguel', 'root', 'hola@hola.es', '12345678'),
(9, 'zavodca', 'miguel', 'root', 'hola@hola.es', '12345678'),
(10, 'zavodca', 'miguel', 'root', 'hola@hola.es', '12345678'),
(11, 'root', 's', 's', 'sss@ss.ss', '12345678'),
(12, 'root', 's', 's', 'sss@ss.ss', '12345678'),
(13, 'zavodca', 'miguel', 'root', 'hola@hola.es', '12345678'),
(14, 'zavodca', 'miguel', 'root', 'hola@hola.es', '12345678'),
(15, 'zavodca', 'asdf', 'root', 'asdf@a.s', '12345678'),
(16, 'zavodca', 'asdf', 'root', 'asdf@a.s', '12345678'),
(17, 'zavodca', 'miguel', 'root', 'asdf@a.s', '12345678'),
(18, 'zavodca', 'miguel', 'root', 'asdf@a.s', '12345678'),
(19, 'hola', '', '', '', ''),
(21, 'ramon', 'ramon', 'el chustas', 'ramoncin@hola.adios', '12345678'),
(22, 'ramon', 'ramon', 'el chustas', 'ramoncin@hola.adios', '12345678'),
(23, 'ramon', 'ramon', 'el chustas', 'ramoncin@hola.adios', '12345678'),
(24, 'paco', 'asdf', 'root', 'asdf@200.asdf', '12345678'),
(25, 'paco', 'asdf', 'root', 'asdf@200.asdf', '12345678'),
(26, 'paco', 'asdf', 'root', 'asdf@200.asdf', '12345678'),
(27, 'paco', 'asdf', 'root', 'asdf@200.asdf', '12345678'),
(28, 'alfredo', 'a', 'root', 'asdf@a.s', '12345678'),
(29, 'ramon2', 'miguel', 'root', 'hola@hola.es', '12345678'),
(30, 'ramon3', 'a', 'root', 'asdf@a.s', 'perro'),
(31, 'paco2', 'asdf', 'root', 'adsf@s.s', '12345678'),
(32, 'perro', 'ramon', 'root', 'asdf@a.s', '12345678'),
(35, 'miguel', 'asdf', 'root', 'asdf@a.s', '12345678'),
(36, 'miguel1', 'miguel', 'root', 'adsf@s.s', '12345678'),
(37, 'miguel2', 'asdf', 'root', 'asdf@200.asdf', '12345678'),
(38, 'pedro', 'pedro', 'root', 'pedro@hola.es', '12345678'),
(39, 'miguel111', 'miguel', 'cuesta', 'miguel@hola.es', '1234'),
(40, 'angel78', 'miguel', 'cuesta', 'miguel@hola.es', '123abc'),
(41, 'cuesta12', 'cuesta', 'mozo', 'cuesta1@hola.es', '123456'),
(42, 'Sandra', 'Sandra', 'CS', 'sandra@hotmail.com', '1234'),
(43, 'miguel1539', 'miguel', 'miguel', 'miguel@hola.es', '1234fun@'),
(44, 'miguel78', 'asdf', 'root', 'asdf@a.s', '123'),
(45, 'miguel788', 'ramon', 'root', 'adsf@s.s', '12345678');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
