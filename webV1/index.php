<!DOCTYPE HTML>
<html>
	<head>
		<title>VirtualENV</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="css/estilos.css" />
		<link rel="icon" href="css/images/favicon.ico">
	</head>
	<body id="fondo">

		<!-- Header -->
			<header id="header" class="alt">
				<h2><a href="index.php"><img  src="css/images/logo4.png" class="logo" width="58" height="56"></a></h2>
				<nav id="nav">
					<ul>

						<!--<li><a href="#">sobre nosotros</a></li>-->
									<?php
									session_start();

									$nombre=$_SESSION['nombre'];

									if ($nombre != "") {
										echo "<li><a href='sobrenosotros.php'>sobre nosotros</a></li>";
										echo "<li> <a href='sshclient.php'>SSH-client</a></li>";
										echo "<li> <a href='logout.php'>logout</a> </li>";
										echo "<li> User: $nombre</li>";
									}else {
										echo "<li><a href='sobrenosotros.php'>sobre nosotros</a></li>";
										echo "<li><a href='login.php'>login</a></li>";

									}
									?>
					</ul>

				</nav>
			</header>
<!-- menu desplegable -->
<!-- esto es lo de javascript que no entiendo, copia y pega-->
			<a href="#menu" class="navPanelToggle"> <input type="button" name="" value="menu" class="button fit small" small> </a>

		<!-- Banner -->
			<section id="banner">

				<div class="logovariant">
					<img src="css/images/logo3.png" >
				</div>

				<p>Ultimas actualizaciones en tecnologia<br /> de Virtual Environment</p>
				<ul>
					<li>
						<?php
						if ($nombre != "") {
							echo '<a href="sshclient.php" class="button  variant">go to SSH-CLIENT</a>';
						}else {
							echo '<a href="login.php" class="button  variant">empieza ahora</a>';
						}
						?>
						</li>
				</ul>
			</section>

			<!-- producto 1 y 2 -->
				<section class="special">
								<header class="major">
									<h2>Nuestra Empresa</h2>
								<p>
									La idea de mi negocio tiene como idea principal es dar servicio de computación en la nube a todos aquel que quiera trabajar con varios ordenadores desde cualquier sistema operativo mientras tenga un navegador o utilizar nuestro servicio secundario que sería el almacenamiento en la nube, ya que está ubicado en nuestros servidores, los cuales trabajan con un sistema de doble confirmación para la seguridad del propio usuario y también un sistema que permite interconectar máquinas virtuales en subredes según las necesidades de la empresa o cliente que soliciten nuestros servicios,  a diferencia de otras empresas la nuestra cuenta con un servicio de atención al cliente personalizado además de un control de acceso a los servidores para que no pueda entrar nadie sin autorización lo que verifica la integridad de los datos, lo que los hace más seguros contra robos de información y protección de datos.
								</p>
								</header>
				</section>
				<div class="bloque">

									<div class="bloque2">

										<blockquote id="block2" >
											<img src="css/images/producto1.png" class="logovariant" id="foto1">

												<h2 class="major">paquete basico</h2>

											A lo largo de las siguientes páginas, expondremos las bases para la creación de una nueva empresa que ofrece un servicio de virtualización en la nube, lo cual permitiría tener una máquina virtual sin necesidad de gastar los recursos de los ordenadores personales, también ofrece un servicio de almacenamiento a parte en la nube. La idea surge a partir de un estudio de mercado sobre la computación en la nube que ha demostrado estar en auge y profundo crecimiento en los últimos años.
											en este paquete inclulle la creacion de un usuario en una maquina debian con 2g ram y 2 nucleos
												<br>
												<?php
												if ($nombre != "") {
													echo '<a href="sshclient.php" class="button small variant" id="boton1" >go to SSH-CLIENT</a>';
												}else {
													echo '<a href="login.php" class="button small variant" id="boton1" >comprar</a>';
												}
												?>


											</blockquote>

									</div>
									</div>
									<div class="bloque">


									<div class="bloque2">

										<blockquote id="block1">
											<img src="css/images/producto1.png" class="logovariant" id="foto2">

												<h2 class="major">paquete avanzado (MANTENIMIENTO)</h2>

								A lo largo de las siguientes páginas, expondremos las bases para la creación de una nueva empresa que ofrece un servicio de virtualización en la nube, lo cual permitiría tener una máquina virtual sin necesidad de gastar los recursos de los ordenadores personales, también ofrece un servicio de almacenamiento a parte en la nube. La idea surge a partir de un estudio de mercado sobre la computación en la nube que ha demostrado estar en auge y profundo crecimiento en los últimos años.
												<br>
												<input type="submit" name="" value="MANTENIMIENTO" class="variant small" id="boton2">
											</blockquote>

									</div>
									</div>


		<!-- Footer -->
			<footer id="footer">
					<ul class="piep">
						<li><a href="https://www.facebook.com/miguel.cuesta.5473/"> <img src="css/images/face.png"/> </a></li>
						<li><a href="https://www.linkedin.com/in/miguel-%C3%A1ngel-c-015231138/"> <img src="css/images/link.png"/> </a></li>
						<li><a href="https://www.instagram.com/miguel_1539/"> <img src="css/images/insta.png" alt=""> </a></li>
					</ul>
					<ul class="piep">
						<li>&copy; Copyright</li>
						<li>Design: <a href="#">Miguel Ángel Cuesta</a></li>
						<li>Images: <a href="www.google.es">Google</a></li>
					</ul>
			</footer>

			<!-- Scripts -->
				<script src="css/js/jquery.min.js"></script>
				<script src="css/js/skel.min.js"></script>
				<script src="css/js/util.js"></script>
				<script src="css/js/main.js"></script>
	</body>
</html>
