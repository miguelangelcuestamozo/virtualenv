<!DOCTYPE HTML>
<html>
	<head>
		<title>VirtualENV</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="css/estilos.css" />
		<link rel="icon" href="css/images/favicon.ico">
		<!-- script para recargar la pagina web ya que el menu no se actualiza a la primera y necesiata regargase -->
		<script type="text/javascript">
			 window.onload = function() { if(!window.location.hash) { window.location = window.location + '#loaded'; window.location.reload(); } }
		</script>
	</head>
	<body id="fondo">
		<!-- Header  -->
			<header id="header" class="alt">
				<h2><a href="index.php"><img  src="css/images/logo4.png" class="logo" width="58" height="56"></a></h2>
				<nav id="nav">
					<ul>

						<?php
						session_start();

						$nombre=$_SESSION['nombre'];

						if ($nombre != "") {
							echo "<li><a href='sobrenosotros.php'>sobre nosotros</a></li>";
							echo "<li> <a href='sshclient.php'>SSH-client</a></li>";
							echo "<li> <a href='logout.php'>logout</a> </li>";
							echo "<li> User: $nombre</li>";
						}else {
							echo "<li><a href='sobrenosotros.php'>sobre nosotros</a></li>";
							echo "<li><a href='login.php'>login</a></li>";

						}
						?>

					</ul>

				</nav>
			</header>
<!-- esto es lo de java que no lo entiendo, copia y pega-->
			<a href="#menu" class="navPanelToggle"> <input type="button" name="" value="menu" class="button fit small" small> </a>

		<!-- Banner -->
			<section id="banner">

				<div class="logovariant">
					<img src="css/images/logo3.png" >
				</div>

				<p>Ultimas actializaciones en tecnologia<br /> de Virtual Environment</p>
				<ul>
					<li><a href="#" class="button variant">empieza ahora</a></li>
				</ul>
			</section>

	 <!-- comentarios only -->
	 <div class="bloque">
		 <table width="1000">
		 	<tr>
		 		<td align="center"> <br> <br> <br> <h2>sesion cerrada correctamente</h2></td>
		 	</tr>
			<tr>
				<td align="center"><br> <br> <br><a href="login.php" class="button fit variant">ir al login</a> <br> <br> <br></td>
			</tr>
		 </table>
		 <?php

			session_start();
			session_destroy();
		 ?>

	 </div>


		<!-- Footer -->
			<footer id="footer">
					<ul class="piep">
						<li><a href="#"> <img src="css/images/face.png"/> </a></li>
						<li><a href="#"> <img src="css/images/link.png"/> </a></li>
						<li><a href="#"> <img src="css/images/insta.png" alt=""> </a></li>
					</ul>
					<ul class="piep">
						<li>&copy; Copyright</li>
						<li>Design: <a href="#">Miguel Ángel Cuesta</a></li>
						<li>Images: <a href="www.google.es">Google</a></li>
					</ul>
			</footer>

			<!-- Scripts -->
				<script src="css/js/jquery.min.js"></script>
				<script src="css/js/skel.min.js"></script>
				<script src="css/js/util.js"></script>
				<script src="css/js/main.js"></script>
	</body>
</html>
