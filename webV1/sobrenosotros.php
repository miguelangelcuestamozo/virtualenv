<!DOCTYPE HTML>
<html>
	<head>
		<title>VirtualENV</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="css/estilos.css" />
		<link rel="icon" href="css/images/favicon.ico">
	</head>
	<body id="fondo">

		<!-- Header -->
			<header id="header" class="alt">
				<h2><a href="index.php"><img  src="css/images/logo4.png" class="logo" width="58" height="56"></a></h2>
				<nav id="nav">
					<ul>

						<!--<li><a href="#">sobre nosotros</a></li>-->
									<?php
									session_start();

									$nombre=$_SESSION['nombre'];

									if ($nombre != "") {
										echo "<li><a href='sobrenosotros.php'>sobre nosotros</a></li>";
										echo "<li> <a href='sshclient.php'>SSH-client</a></li>";
										echo "<li> <a href='logout.php'>logout</a> </li>";
										echo "<li> User: $nombre</li>";
									}else {
										echo "<li><a href='sobrenosotros.php'>sobre nosotros</a></li>";
										echo "<li><a href='login.php'>login</a></li>";

									}
									?>
					</ul>

				</nav>
			</header>
<!-- menu desplegable -->
<!-- esto es lo de javascript que no entiendo, copia y pega-->
			<a href="#menu" class="navPanelToggle"> <input type="button" name="" value="menu" class="button fit small" small> </a>
			<!-- Banner -->
				<section id="banner">

					<div class="logovariant">
						<img src="css/images/logo3.png" >
					</div>

					<p>Ultimas actializaciones en tecnologia<br /> de Virtual Environment</p>
					<ul>
						<?php
						if ($nombre != "") {
							echo '<a href="sshclient.php" class="button  variant">go to SSH-CLIENT</a>';
						}else {
							echo '<a href="login.php" class="button  variant">empieza ahora</a>';
						}
						?>
					</ul>
				</section>
		<!-- texto -->
		<section class="special">
						<header class="major">
							<h2>sobre Nuestra Empresa</h2>
						<p>
							La idea de mi negocio tiene como objetivo principal proporcionar un servicio
							de computación en la nube a través de un navegador on-line a todos aquellos
							que deseen o necesiten trabajar con varios dispositivos simultáneamente desde
							cualquier sistema operativo o bien, quieran utilizar nuestro servicio secundario,
							que sería el almacenamiento en la nube. Dicho almacenamiento, está ubicado
							en nuestros servidores, los cuales trabajan con un sistema de doble
							confirmación para la seguridad del propio usuario y también cuentan con un
							sistema que permite interconectar máquinas virtuales en subredes según las
							necesidades de la empresa o cliente que solicite nuestros servicios. A diferencia
							de otras empresas, la nuestra cuenta con un Servicio de Atención al Cliente
							personalizado, además de un control de acceso a los servidores para que no
							pueda obtener acceso alguien que no esté autorizado, lo que verifica la
							integridad de los datos permitiendo garantizar seguridad contra robos de
							información y la protección de datos.
						</p>
						</header>
		</section>
		<section class="special">
						<header class="major">
							<h2>ubicacion</h2>
						<p>
							<img src="css/images/ubi.png" alt="">
						</p>
						</header>
		</section>
		<section class="special">
						<header class="major">
							<h2>Cultura empresarial: misión, visión y valores.</h2>
						<p>
							Misión: ofrecer productos y servicios de alta calidad al conste más competitivo
							posible. <br>
							Visión: extender el alcance de la empresa al mercado internacional en un futuro
							no demasiado lejano y poder liderar con ejemplo de virtud el mercado
							informático del cual prestamos servicios. <br>
							Valores: eficiencia, integridad y desarrollo, buscando la mejora personal y
							empresarial.
						</p>
						</header>
		</section>
		<!-- Footer -->
			<footer id="footer">
					<ul class="piep">
						<li><a href="https://www.facebook.com/miguel.cuesta.5473/"> <img src="css/images/face.png"/> </a></li>
						<li><a href="https://www.linkedin.com/in/miguel-%C3%A1ngel-c-015231138/"> <img src="css/images/link.png"/> </a></li>
						<li><a href="https://www.instagram.com/miguel_1539/"> <img src="css/images/insta.png" alt=""> </a></li>
					</ul>
					<ul class="piep">
						<li>&copy; Copyright</li>
						<li>Design: <a href="#">Miguel Ángel Cuesta</a></li>
						<li>Images: <a href="www.google.es">Google</a></li>
					</ul>
			</footer>

			<!-- Scripts -->
				<script src="css/js/jquery.min.js"></script>
				<script src="css/js/skel.min.js"></script>
				<script src="css/js/util.js"></script>
				<script src="css/js/main.js"></script>
	</body>
</html>
