# VirtualENV

**REQUISITOS**

- Tener instalado virtualbox.
- Instalar una maquina virtual con linux da igual si es interfaz grafica o no. (mas senillo con interfaz grafica)
- Instalar en la maquina shellinabox.
- Quitar el SSL en el archivo de configuración de shellinabox.
- Un editor de texto para editar el codigo.
- Instalar las librerias libssh2-1, libssh2-1-dev y php-ssh2.
- Instalar un xampp o apache2, mysql y es opcional pero muy recomendado el phpmyadmin.


**IMPLEMENTACIÓN**

- Lo primero sería descargar el repositorio dentro de la carpeta de nuestro servidor web en mi caso /var/www/html.
- Después ejecutar importar la base de datos dejada en la carpeta sql.
- Modificar la página web (shhclient.php) el iframe con la url de vuestra maquina creada.
- Modificar la página web (login.php) en apartado de la conexión ssh poner el usuario administrado con la contraseña de vuestras maquina también la ip.


**COMPORBAR FUNCIONAMIENTO**

Crear usuario en el apartado login y ver si se actualiza la base de datos y la máquina virtual, después de eso ver si el apartado de sshclient.php funciona correctamente y sale la conexion ssh con usuario y contraseña.

